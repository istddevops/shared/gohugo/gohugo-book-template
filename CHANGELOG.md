# Changelog

Alle belangrijke wijzigingen worden in dit bestand vastgelegd.

Het gebruikte formaat is gebaseerd op [Houd een Changelog bij 1.0.0](https://keepachangelog.com/nl/1.0.0),
en gaat uit van [Semantisch Versioneren 2.0.0](https://semver.org/lang/nl/#semantisch-versioneren-200).


## [0.1.0] - 2022-03-09

### Changed

- Content Params / BookSection veranderd naar `test` zodat deze de test-content niet zal meenemen in de projecten die het Template gebruiken
- CI-script aangepast met standaard `dev` validatie en `main` pages (vanuit de gedeelde GoHuGo CI-script)

## [0.0.1] - 2022-02-18

### Added

- Voorbereiding van verkenning naar experimentele toepassing (voorbereiding toepassing Afsprakensets KIK-V en iWlz registers e.d.)
- Deze CHANGELOG.md om te komen tot een gestructureerd versiebeheer volgens semantisch versioneren.
- Config, content, package.json en .gitlab-ci.yml toegevoegd voor het lokaal testen en publicatie van voorbeelden
- KROKI-Shortcode aangepast voor verbeterde performance en stabiliteit (rest-api get / document.write cunstructie vervangen door inline HTML-object)
