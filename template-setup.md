# HUGO Book Template

Dit standaard KIK-V<sup>2</sup> Publicatie Book template maakt gebruik van het [HUGO Book Theme](https://themes.gohugo.io/themes/hugo-book). Aanvullend daarop zijn de volgende aanpassingen gemaakt:

- [HUGO SSG Assets](assets.md)
- [HUGO SSG ShortCodes](shortcodes.md)
- [HUGO SSG Static](static.md)

## Geraadpleegde bronnen

- [Hugo Translator a cheat sheet](https://www.regisphilibert.com/blog/2017/04/hugo-cheat-sheet-go-template-translator)
- [How to Customise Tables](https://discourse.gohugo.io/t/how-to-customise-tables/15661)


- hugo mod init gitlab.com/istddevops/shared/hugo-ssg/forestry-book-template