# HUGO SSG / Book template / Layouts / ShortCodes

Uitbreidingen en/of aanpassingen op de bij [HUGO meegeleverde ShortCodes](https://gohugo.io/content-management/shortcodes/#use-hugos-built-in-shortcodes) en [Book Theme ShortCodes](https://themes.gohugo.io/themes/hugo-book/#shortcodes).

## Kroki

Via de [KROKI API](https://kroki.io/) kunnen *diagrammen* vanuit *tekst-codering* worden weergegeven. In een HUGO Book publicatie kan een *Kroki Shortcode* worden gebruikt om een *Kroki-diagram* binnen *markdown* te coderen. Zie onderstaande voorbeeld.

Voorbeeld van een *tekst-codering* voor een *PlantUML-diagram* weergave:

````tpl
{{< kroki >}} 
```plantuml
@startuml  
"HUGO Book" -> "KROKI Shortcode" :  tekst-codering  
"KROKI Shortcode" -> "KROKI API" : opvragen weergave  
"KROKI Shortcode" <- "KROKI API": ontvangen weergave  
"HUGO Book" <- "KROKI Shortcode": diagram weergave  
@enduml  
```
{{< /kroki >}}
````

De *publicatie-weergave* van bovenstaande voorbeeld:
  
```plantuml
@startuml
"HUGO Book" -> "KROKI Shortcode" :  tekst-codering
"KROKI Shortcode" -> "KROKI API" : opvragen weergave
"KROKI Shortcode" <- "KROKI API": ontvangen weergave
"HUGO Book" <- "KROKI Shortcode": diagram weergave
@enduml
```

##
